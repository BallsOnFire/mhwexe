#pragma once
#include <WinSock2.h>
namespace ProxyXInput9_1_0
{
	HINSTANCE originalDllBase{};
	extern "C"
	{
		void* AddrDllMain;
		void* AddrXInputGetCapabilities;
		void* AddrXInputGetDSoundAudioDeviceGuids;
		void* AddrXInputGetState;
		void* AddrXInputSetState;
		void JmpDllMain();
		void JmpXInputGetCapabilities();
		void JmpXInputGetDSoundAudioDeviceGuids();
		void JmpXInputGetState();
		void JmpXInputSetState();
	}
	bool attach()
	{
		originalDllBase = LoadLibraryA(R"(C:\windows\system32\XInput9_1_0.dll)");
		if (!originalDllBase) return false;
		AddrDllMain = GetProcAddress(originalDllBase, "DllMain");
		AddrXInputGetCapabilities = GetProcAddress(originalDllBase, "XInputGetCapabilities");
		AddrXInputGetDSoundAudioDeviceGuids = GetProcAddress(originalDllBase, "XInputGetDSoundAudioDeviceGuids");
		AddrXInputGetState = GetProcAddress(originalDllBase, "XInputGetState");
		AddrXInputSetState = GetProcAddress(originalDllBase, "XInputSetState");
		return (AddrDllMain && AddrXInputGetCapabilities && AddrXInputGetDSoundAudioDeviceGuids && AddrXInputGetState && AddrXInputSetState);
	}
	bool detach()
	{
		FreeLibrary(originalDllBase);
		return true;
	}
}