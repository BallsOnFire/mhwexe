#pragma once
#include <filesystem>
#include <vector>

#include "Json.h"
#include "File.h"
#include "Process.h"

namespace MhwExe
{
	struct Settings
	{
		struct Item
		{
			enum class Type { EXE, DLL };
			enum class LoadMethod { Normal };
		private:
			std::filesystem::path m_path;
			Type m_type;
			std::wstring m_commandLine;
			LoadMethod m_loadMethod;
			bool m_valid;
		public:
			Item(std::wstring path, std::wstring commandLine, const std::wstring& /*loadMethod*/) :m_path{ std::move(path) }, m_commandLine{ std::move(commandLine) }, m_loadMethod{ LoadMethod::Normal }, m_valid{ true }
			{
				std::wstring extension{ m_path.extension().wstring() };
				if (extension == L".exe")m_type = Type::EXE;
				else if (extension == L".dll")m_type = Type::DLL;
				else
				{
					writeln("invalid extension = ", extension);
					m_valid = false;
				}
			}
			bool valid()const { return m_valid; }
			Type type()const { return m_type; }
			const std::filesystem::path& path()const { return m_path; }
			const std::wstring& commandLine()const { return m_commandLine; }
			LoadMethod loadMethod()const { return m_loadMethod; }

			template<class Ch, class Tr>
			friend auto& operator<<(std::basic_ostream<Ch, Tr>& os, const Item& item)
			{
				if (!item.valid())os << "Invalid Item";
				else
				{
					os << "[";
					switch (item.type())
					{
						case Type::EXE: { os << "EXE"; break; }
						case Type::DLL: { os << "DLL"; break; }
					}
					os << "]";
					os << "Path = " << item.path();
					if (!item.commandLine().empty())os << ", CommandLine = " << item.commandLine();
				}
				return os;
			}
		};
	private:
		std::vector<Item> m_items;
		bool m_bAutoClose;
	public:
		Settings(const std::filesystem::path& configFile) :m_items{}, m_bAutoClose{}
		{
			std::wstring errMsg;
			std::wstring fileString{ readUtf8File(configFile) };
			writeln("File = ", fileString);
			Json::Value js{ Json::parse(fileString,&errMsg) };

			if (!errMsg.empty())
			{
				writeln("Fail to parse json, error msg = ", errMsg);
				return;
			}
			else
				writeln("Json = ", js);

			if (!js.isObject()) { writeln("Json value is not object"); return; }

			const Json::Object& obj{ js.asObject() };

			const std::pair<Json::String, Json::Value>* pPairItems{ obj.find(L"items") }; if (!pPairItems) { writeln("Items not found"); return; }
			if (!pPairItems->second.isList()) { writeln("items is not list"); return; }
			const Json::List& itemList{ pPairItems->second.asList() };
			writeln("item list size = ", itemList.size());
			for (const Json::Value& v : itemList)
			{
				writeln("Parsing item = ", v);
				if (!v.isObject()) { writeln("current item is not object"); continue; }

				const Json::Object& itemObj{ v.asObject() };

				const std::pair<Json::String, Json::Value>* pPairPath{ itemObj.find(L"path") };
				if (!pPairPath) { writeln("current item has no path"); continue; }
				const std::pair<Json::String, Json::Value>* pPairCommandLine{ itemObj.find(L"cmd") };
				const std::pair<Json::String, Json::Value>* pPairLoadMethod{ itemObj.find(L"loadMethod") };

				std::wstring path{ pPairPath&&pPairPath->second.isString() ? pPairPath->second.asString() : std::wstring{} };
				std::wstring commandLine{ pPairCommandLine&&pPairCommandLine->second.isString() ? pPairCommandLine->second.asString() : std::wstring{} };
				std::wstring loadMethod{ pPairLoadMethod&&pPairLoadMethod->second.isString() ? pPairLoadMethod->second.asString() : std::wstring{} };

				Item item{ path, commandLine, loadMethod };
				if (item.valid())
					m_items.push_back(item);
				else
					writeln("current item is invalid");
			}

			const std::pair<Json::String, Json::Value>* pPairAutoClose{ obj.find(L"bAutoClose") }; if (!pPairAutoClose) { writeln("bAutoClose not found"); return; }
			if (pPairAutoClose&&pPairAutoClose->second.isBool())m_bAutoClose = pPairAutoClose->second.asBool();

			writeln("bAutoClose = ", m_bAutoClose);
		}

		const std::vector<Item>& items()const { return m_items; }
		bool autoClose()const { return m_bAutoClose; }

		template<class Ch, class Tr>
		friend auto& operator<<(std::basic_ostream<Ch, Tr>& os, const Settings& settings)
		{
			for (const auto& item : settings.items())
				os << item << '\n';

			return os;
		}
	};

	std::filesystem::path getExePath()
	{
		wchar_t result[MAX_PATH + 1];
		GetModuleFileNameW(nullptr, result, MAX_PATH + 1);
		return result;
	}
	std::filesystem::path getExeDir() { return getExePath().parent_path(); }

	//DWORD exeByShell(const std::filesystem::path& exeName, const wchar_t* parameters = nullptr)
	//{
	//	if (!std::filesystem::exists(exeName))return 0;
	//	std::wstring extension{ exeName.extension() };
	//	if (!(extension.size() == 4 && extension[0] == L'.' && (extension[1] == L'e' || extension[1] == L'E') && (extension[2] == L'x' || extension[2] == L'X') && (extension[3] == L'e' || extension[3] == L'E')))
	//		return 0;
	//	SHELLEXECUTEINFOW se{
	//		sizeof(SHELLEXECUTEINFOW),					//DWORD cbSize;               // in, required, sizeof of this structure
	//		SEE_MASK_NOCLOSEPROCESS ,					//ULONG fMask;                // in, SEE_MASK_XXX values
	//		{},											//HWND hwnd;                  // in, optional
	//		nullptr,									//LPCWSTR  lpVerb;            // in, optional when unspecified the default verb is choosen
	//		exeName.c_str(),							//LPCWSTR  lpFile;            // in, either this value or lpIDList must be specified
	//		parameters,									//LPCWSTR  lpParameters;      // in, optional
	//		nullptr,									//LPCWSTR  lpDirectory;       // in, optional
	//		SW_SHOW,									//int nShow;                  // in, required
	//		nullptr,									//HINSTANCE hInstApp;         // out when SEE_MASK_NOCLOSEPROCESS is specified
	//		nullptr,									//void *lpIDList;             // in, valid when SEE_MASK_IDLIST is specified, PCIDLIST_ABSOLUTE, for use with SEE_MASK_IDLIST & SEE_MASK_INVOKEIDLIST
	//		nullptr,									//LPCWSTR  lpClass;           // in, valid when SEE_MASK_CLASSNAME is specified
	//		nullptr,									//HKEY hkeyClass;             // in, valid when SEE_MASK_CLASSKEY is specified
	//		0,											//DWORD dwHotKey;             // in, valid when SEE_MASK_HOTKEY is specified
	//		{},											//Union
	//		{}											//HANDLE hProcess;            // out, valid when SEE_MASK_NOCLOSEPROCESS specified
	//	};
	//	ShellExecuteExW(&se);
	//	DWORD pid{ GetProcessId(se.hProcess) };
	//	if (se.hProcess)CloseHandle(se.hProcess);
	//	return pid;
	//}

	//DWORD exeByCreateProcess(const std::filesystem::path& exeName, const std::wstring& commandLine, DWORD flags = CREATE_NEW_CONSOLE)
	//{
	//	STARTUPINFOW si{}; si.cb = sizeof(si);
	//	PROCESS_INFORMATION pi{};
	//	std::wostringstream wss;
	//	wss << '"' << exeName.c_str() << "\" " << commandLine;
	//	std::wstring str{ wss.str() };
	//	writeln(str);
	//	CreateProcessW(
	//		nullptr,				// the path
	//		&str[0],				// Command line
	//		nullptr,				// Process handle not inheritable
	//		nullptr,				// Thread handle not inheritable
	//		FALSE,					// Set handle inheritance to FALSE
	//		flags,					// flags
	//		nullptr,				// Use parent's environment block
	//		nullptr,				// Use parent's starting directory 
	//		&si,					// Pointer to STARTUPINFO structure
	//		&pi						// Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
	//	);
	//	CloseHandle(pi.hProcess);
	//	CloseHandle(pi.hThread);
	//	return pi.dwProcessId;
	//}
	//void exeByCmd(const std::filesystem::path& exeName, const wchar_t* commandLine = nullptr)
	//{
	//	std::wostringstream ss;
	//	ss << LR"(start "" ")";
	//	ss << exeName.c_str() << '"';
	//	if (commandLine)
	//		ss << L' ' << commandLine;
	//	_wsystem(ss.str().c_str());
	//}

	DWORD exeByRemoteCreateProcess(const Windows::Process& process, const std::filesystem::path& exeName, const wchar_t* commandLine = nullptr)
	{
		return process.createProcess64(exeName, commandLine);
	}


	void start(const Settings& settings)
	{
		writeln("settings = ", settings);

		Windows::Process explorer;
		std::vector<PROCESSENTRY32W> entries{ Windows::Process::getEntriesByName(L"explorer.exe") };
		writeln("Found ", entries.size(), " Processes named 'explorer.exe'");

		if (!(entries.size() == 1))return;

		if (!explorer.loadByEntry(entries[0])) { writeln("fail to load 'explorer.exe'"); }
		writeln("'explorer.exe' is loaded");
		for (const Settings::Item& item : settings.items())
			switch (item.type())
			{
				case Settings::Item::Type::EXE:
				{
					//writeln("Exe = ",item.path(),", PID = ", exeAsChild(item.path(), item.commandLine()));
					//writeln("Exe = ", item.path(), ", PID = ", exeByShell(item.path(), item.commandLine().c_str()));
					//exeByCmd(item.path(), item.commandLine().c_str());
					writeln("Launching ", item.path(), ", PID = ", exeByRemoteCreateProcess(explorer, item.path(), item.commandLine().c_str()));
					break;
				}
				case Settings::Item::Type::DLL:
				{
					break;
				}
			}
	}
	void end(const Settings& settings)
	{
		if (settings.autoClose())
		{
			writeln("Done");
			Sleep(3000);
		}
		else
			system("pause");
	}

	void run()
	{
		const Settings settings{ getExeDir() / L"ExeConfig.txt" };
		start(settings);
		end(settings);
	}
}