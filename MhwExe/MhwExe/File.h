#pragma once
#include <string>
#include <filesystem>
#include <sstream>
#include <fstream>
#include <windows.h>

template<class char_t, class = std::enable_if_t<std::is_same_v<char_t, char> || std::is_same_v<char_t, unsigned char>>>
std::wstring utf8ToUtf16(const char_t* pBegin, const char_t* pEnd)
{
	const std::ptrdiff_t size{ pEnd - pBegin };

	int nLenWide{ MultiByteToWideChar(CP_UTF8, 0, reinterpret_cast<const char*>(pBegin),static_cast<int>(size), 0, 0) };

	std::wstring result(nLenWide, 0);

	if (MultiByteToWideChar(CP_UTF8, 0, reinterpret_cast<const char*>(pBegin), static_cast<int>(size), &result[0], nLenWide) != nLenWide)
		return L"";

	return result;
}
std::vector<uint8_t> file2chars(const std::filesystem::path& filename)
{
	std::ifstream input{ filename, std::ios_base::binary };
	return { std::istreambuf_iterator<char>{input}, std::istreambuf_iterator<char>{} };
}
std::wstring readUtf8File(const std::filesystem::path& filename)
{
	std::vector<uint8_t> chars{ file2chars(filename) };
	if (!chars.size())return {};
	const uint8_t* pBegin{ (chars.size() >= 3 && chars[0] == 0xEF && chars[1] == 0xBB && chars[2] == 0xBF) ? &chars[0] + 3 : &chars[0] };
	const uint8_t* pEnd{ &chars[0] + chars.size() };
	return utf8ToUtf16(pBegin, pEnd);
}