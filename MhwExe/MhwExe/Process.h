#pragma once
#include <winsock2.h>
#include <Psapi.h> //GetModuleBaseNameW 
#include <TlHelp32.h>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include <type_traits>
#include <iomanip>
#include <functional>
#include <string>
#include <sstream>
#include <filesystem>
#include <array>

template<class T, class U>
constexpr decltype(auto) truncate(const U& value)
{
	return *reinterpret_cast<const T*>(&value);
}

namespace Windows
{
	struct Process
	{
	private:
		std::wstring m_name;
		DWORD m_pid;
		DWORD m_parentPid;
		HANDLE m_handle;
		bool m_is64;
	public:
		Process() :m_name{}, m_pid{ 0 }, m_parentPid{ 0 }, m_handle{ INVALID_HANDLE_VALUE }, m_is64{ false } {}
		~Process() { dispose(); }
		Process(Process&&) = delete;
		Process(const Process&) = delete;
		Process& operator=(Process&&) = delete;
		Process& operator=(const Process&) = delete;

		const HANDLE& handle()const { return m_handle; }
		const std::wstring& name()const { return m_name; }
		DWORD pid()const { return m_pid; }
		DWORD parentPid()const { return m_parentPid; }
		bool is64()const { return m_is64; }

		uintptr_t alloc(size_t sizeInBytes, uintptr_t preferedBase = 0)const
		{
			return reinterpret_cast<uintptr_t>(VirtualAllocEx(m_handle, reinterpret_cast<void*>(preferedBase), sizeInBytes, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE));
		}

		bool free(uintptr_t address, size_t sizeInBytes)const
		{
			return VirtualFreeEx(m_handle, reinterpret_cast<void*>(address), sizeInBytes, MEM_RELEASE);
		}

		template<size_t Bits>
		struct HeapMemory
		{
			using ptr_t = std::conditional_t<Bits == 64, uint64_t, uint32_t>;
			using size_type = std::conditional_t<Bits == 64, uint64_t, uint32_t>;
		private:
			const Process& m_process;
			size_type m_size;
			ptr_t m_address;
		public:

			HeapMemory(const Process& process, size_type size, ptr_t preferedBase = 0) :m_process{ process }, m_size{ size }, m_address{ process.alloc(size,preferedBase) } {}
			HeapMemory(const Process& process, const void* pPayload, size_type size, ptr_t preferedBase = 0) :m_process{ process }, m_size{ size }, m_address{ m_size ? truncate<size_type>(process.alloc(m_size,preferedBase)) : 0 } { if (m_size && !process.write(m_address, pPayload, m_size))throw 0; }

			HeapMemory(const Process& process, const wchar_t* str, ptr_t preferedBase = 0) :HeapMemory{ process,str,str ? truncate<size_type>((wcslen(str) + 1) * sizeof(wchar_t)) : 0,preferedBase } {}
			HeapMemory(const Process& process, const char* str, ptr_t preferedBase = 0) :HeapMemory{ process,str,str ? truncate<size_type>((strlen(str) + 1) * sizeof(char)) : 0,preferedBase } {}
			HeapMemory(const Process& process, const std::filesystem::path& path, ptr_t preferedBase = 0) :HeapMemory{ process,path.c_str(),preferedBase } {}

			template<class T, class = std::enable_if_t<!std::is_pointer_v<T>> >
			HeapMemory(const Process& process, const T& data, ptr_t preferedBase = 0) : HeapMemory{ process,&data,sizeof(data),preferedBase } {}

			~HeapMemory() { if (m_address) { m_process.free(m_address, m_size); m_address = 0; } }

			ptr_t address()const { return m_address; }
			size_type size()const { return m_size; }
		private:
			HeapMemory(const HeapMemory&) = delete;
			HeapMemory(HeapMemory&&) = delete;
			HeapMemory& operator=(const HeapMemory&) = delete;
			HeapMemory& operator=(HeapMemory&&) = delete;
		};

		bool loadByEntry(const PROCESSENTRY32W& entry, uint32_t* pErrorID = nullptr)
		{
			dispose();
			m_pid = entry.th32ProcessID;
			m_parentPid = entry.th32ParentProcessID;
			m_name = entry.szExeFile;
			m_handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);
			if (m_handle == 0)
			{
				if (pErrorID)*pErrorID = GetLastError();
				m_handle = INVALID_HANDLE_VALUE;
				return false;
			}

			BOOL is64;
			if (!IsWow64Process(m_handle, &is64))
			{
				if (pErrorID)*pErrorID = GetLastError();
				dispose();
				return false;
			}
			m_is64 = is64 == 0;

			return true;
		}

		template<class T> bool write(uintptr_t address, const T& input)const
		{
			static_assert(!std::is_pointer_v<T>);
			return WriteProcessMemory(m_handle, reinterpret_cast<void*>(address), &input, sizeof(input), nullptr);
		}
		template<class T> bool write(uintptr_t address, const T* pInput, size_t sizeInBytes)const
		{
			return WriteProcessMemory(m_handle, reinterpret_cast<void*>(address), (void*)(pInput), sizeInBytes, nullptr);
		}

		template<class T> bool read(uintptr_t address, T& output)const
		{
			static_assert(!std::is_pointer_v<T>);
			return ReadProcessMemory(m_handle, reinterpret_cast<void*>(address), &output, sizeof(T), nullptr);
		}
		template<class T> bool read(uintptr_t address, T* pOutput, size_t sizeInBytes)const
		{
			return ReadProcessMemory(m_handle, reinterpret_cast<void*>(address), (void*)(pOutput), sizeInBytes, nullptr);
		}

		bool createThread(uintptr_t rpFunc, uintptr_t rpParam)const
		{
			HANDLE remoteThread{ CreateRemoteThread(
				m_handle,												//_In_ HANDLE m_handle,
				nullptr,												//_In_opt_ LPSECURITY_ATTRIBUTES lpThreadAttributes,
				0,														//_In_ SIZE_T dwStackSize,
				reinterpret_cast<LPTHREAD_START_ROUTINE>(rpFunc),		//_In_ LPTHREAD_START_ROUTINE lpStartAddress,
				reinterpret_cast<void*>(rpParam),						//_In_opt_ LPVOID lpParameter,
				0,														//_In_ DWORD dwCreationFlags,
				nullptr													//_Out_opt_ LPDWORD lpThreadId
			) };
			if (!remoteThread)return false;
			WaitForSingleObject(remoteThread, INFINITE);
			CloseHandle(remoteThread);
			return true;
		}

		static std::vector<PROCESSENTRY32W> getEntriesByName(const wchar_t*targetProcessName)
		{
			return getEntries([&](const PROCESSENTRY32W& pe) { return wcscmp(targetProcessName, pe.szExeFile) == 0; });
		}

		template<class Lambda>
		static std::vector<PROCESSENTRY32W> getEntries(Lambda&& fIsMatch)
		{
			std::vector<PROCESSENTRY32W> result;
			HANDLE handleSnap{ CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0) };
			if (handleSnap == INVALID_HANDLE_VALUE)
				return result;
			PROCESSENTRY32W pe32; pe32.dwSize = sizeof(pe32);
			if (!Process32FirstW(handleSnap, &pe32))
			{
				CloseHandle(handleSnap);
				return result;
			}

			do
			{
				if (fIsMatch(pe32))
					result.push_back(pe32);
			} while (Process32NextW(handleSnap, &pe32));
			CloseHandle(handleSnap);
			return result;
		}

		DWORD createProcess64(const std::filesystem::path& path, const wchar_t* commandLine = nullptr, const wchar_t* currentDirectory = nullptr, DWORD flags = CREATE_NEW_CONSOLE)const
		{
			struct Param
			{
				uint64_t pCreateProcessW;
				uint64_t pCloseHandle;
				uint64_t exePath;
				uint64_t commandLine;
				uint64_t currentDirectory;
				DWORD flags;
				DWORD resultPid;
				DWORD resultTid;
				DWORD resultFlag;
			};
			constexpr std::array<uint8_t, 236> CreateProcessWShellCode64{ 0x48,0x85,0xC9,0x0F,0x84,0xE2,0x00,0x00,0x00,0x48,0x89,0x5C,0x24,0x08,0x55,0x48,0x8D,0x6C,0x24,0xA9,0x48,0x81,0xEC,0xE0,0x00,0x00,0x00,0x48,0x8B,0xD9,0xC7,0x45,0xE7,0x68,0x00,0x00,0x00,0x33,0xC9,0x48,0x8D,0x45,0xC7,0x48,0x89,0x44,0x24,0x48,0x0F,0x57,0xC0,0x48,0x8D,0x45,0xE7,0x48,0x89,0x4D,0xEF,0x4C,0x8B,0x13,0x0F,0x57,0xC9,0x48,0x8B,0x53,0x18,0x45,0x33,0xC9,0x48,0x89,0x44,0x24,0x40,0x45,0x33,0xC0,0x48,0x8B,0x43,0x20,0x48,0x89,0x44,0x24,0x38,0x8B,0x43,0x28,0x48,0x89,0x4C,0x24,0x30,0x89,0x44,0x24,0x28,0x89,0x4C,0x24,0x20,0x48,0x89,0x4D,0x07,0x48,0x89,0x4D,0x0F,0x48,0x89,0x4D,0x17,0x48,0x89,0x4D,0x1F,0x89,0x4D,0x27,0x48,0x89,0x4D,0x2F,0x48,0x89,0x4D,0x47,0x48,0x89,0x4D,0xD7,0x48,0x8B,0x4B,0x10,0x66,0x0F,0x7F,0x45,0xF7,0x66,0x0F,0x7F,0x45,0x37,0xF3,0x0F,0x7F,0x4D,0xC7,0x41,0xFF,0xD2,0x85,0xC0,0x75,0x18,0xC7,0x43,0x34,0x02,0x00,0x00,0x00,0x48,0x8B,0x9C,0x24,0xF0,0x00,0x00,0x00,0x48,0x81,0xC4,0xE0,0x00,0x00,0x00,0x5D,0xC3,0x48,0x8B,0x4D,0xCF,0xFF,0x53,0x08,0x48,0x8B,0x4D,0xC7,0xFF,0x53,0x08,0x8B,0x45,0xD7,0x89,0x43,0x2C,0x8B,0x45,0xDB,0x89,0x43,0x30,0xC7,0x43,0x34,0x01,0x00,0x00,0x00,0x48,0x8B,0x9C,0x24,0xF0,0x00,0x00,0x00,0x48,0x81,0xC4,0xE0,0x00,0x00,0x00,0x5D,0xC3 };
			constexpr DWORD flagUnfinished{ 0 };
			constexpr DWORD flagSucceed{ 1 };
			constexpr DWORD flagFailed{ 2 };

			HeapMemory<64> hmPath{ *this,path };
			HeapMemory<64> hmCmd{ *this,commandLine };
			HeapMemory<64> hmDir{ *this,currentDirectory };
			HeapMemory<64> hmShellCode{ *this,CreateProcessWShellCode64 };

			Param param{
				reinterpret_cast<uintptr_t>(&::CreateProcessW),
				reinterpret_cast<uintptr_t>(&::CloseHandle),
				hmPath.address(),
				hmCmd.address(),
				hmDir.address(),
				flags,
				0,
				0,
				flagUnfinished
			};

			HeapMemory<64> hmParam{ *this,param };

			if (!createThread(hmShellCode.address(), hmParam.address()))return 0;

			for (uint32_t count{}; count != 1000; ++count)//TODO
			{
				Param buff;
				if (!read(hmParam.address(), buff))return 0;
				if (buff.resultFlag == flagSucceed)
					return buff.resultPid;
				else if (buff.resultFlag == flagFailed)
					return 0;
			}
			return 0;
		}

	private:

		void dispose()
		{
			if (m_handle != INVALID_HANDLE_VALUE)
			{
				CloseHandle(m_handle);
				m_handle = INVALID_HANDLE_VALUE;
			}
		}
	};
}

