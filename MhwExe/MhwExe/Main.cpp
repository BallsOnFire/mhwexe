#include <WinSock2.h>
#include "Console.h"
#include "Proxy.h"
#include "MhwExe.h"

DWORD __stdcall attach(void*)
{
	Sleep(5000);
	Windows::Console console{};

	MhwExe::run();

	return 0;
}

BOOL WINAPI DllMain(HINSTANCE, DWORD reason, void*)
{
	switch (reason)
	{
		case DLL_PROCESS_ATTACH:
		{
			if (!ProxyXInput9_1_0::attach())return false;
			CreateThread(0, 0, attach, 0, 0, 0);
			break;
		}
		case DLL_PROCESS_DETACH:
		{
			if (!ProxyXInput9_1_0::detach())return false;
			break;
		}
	}

	return 1;
}