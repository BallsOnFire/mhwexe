#pragma once
#include <sstream>
#include <string>
#include <array>
namespace ct
{
	template<class T, T...Cs>
	struct String
	{
	private:
		using base_t = String<T, Cs...>;
	public:
		using char_type = T;
		static constexpr size_t Size{ sizeof...(Cs) };
		static constexpr std::array<T, Size> chars{ Cs... };

		template<size_t Index>
		static constexpr T at()
		{
			static_assert(Index < Size);
			return chars[Index];
		}

		static std::basic_string<T> str() noexcept { return { Cs... }; }

		template<class Ch, class Tr>
		friend auto& operator<<(std::basic_ostream<Ch, Tr>& os, base_t)
		{
			((os << Cs), ...);
			return os;
		}
	};

	template<class T, T...lcs, T...rcs> constexpr String<T, lcs..., rcs...> operator+(String<T, lcs...>, String<T, rcs...>)noexcept { return{}; }

	template<class char_type, class lambda_t, size_t...I>
	constexpr auto make_cx_string(lambda_t lambda[[maybe_unused]], std::index_sequence<I...>)
	{
		return String<char_type, lambda()[I]...>{};
	}
}

#define XS(str) (ct::make_cx_string<char>([]()constexpr{return(str);},std::make_index_sequence<sizeof(str)/sizeof(char) - 1>{}))
#define WXS(str) (ct::make_cx_string<wchar_t>([]()constexpr{return(str);},std::make_index_sequence<sizeof(str)/sizeof(wchar_t) - 1>{}))

template<class T>struct is_cx_string : std::false_type {};
template<class T, T ... c>struct is_cx_string<ct::String<T, c...>> : std::true_type {};
template<class T>constexpr bool is_cx_string_v = is_cx_string<std::decay_t<T>>::value;