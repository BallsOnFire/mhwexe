#pragma once
#include <variant>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip> //std::setw std::setfill

#include <stdint.h>
#include "CtString.h"

namespace numeric_details
{
	bool isInt(float v) { return ((long long)(v) == v); }
	bool isInt(double v) { return ((long long)(v) == v); }
	bool isInt(long double v) { return ((long long)(v) == v); }
}

template<class Key, class Value>
struct SeqMap
{
	using KeyValuePair = std::pair<Key, Value>;
	using const_iterator = typename std::vector<KeyValuePair>::const_iterator;
	using iterator = typename std::vector<KeyValuePair>::iterator;
	std::vector<KeyValuePair> m_data;

	SeqMap() :m_data{} {}

	SeqMap(std::initializer_list<KeyValuePair> list) :m_data{ list } {}

	const KeyValuePair* find(const Key& key)const { for (const KeyValuePair& pair : m_data)if (pair.first == key)return &pair; return nullptr; }
	KeyValuePair* find(const Key& key) { for (KeyValuePair& pair : m_data)if (pair.first == key)return &pair; return nullptr; }

	Value& operator[](const Key& key)
	{
		KeyValuePair* pPair{ find(key) };
		if (pPair)return pPair->second;
		m_data.emplace_back(key, Value{});
		return m_data.back().second;
	}
	const Value& operator[](const Key& key)const
	{
		KeyValuePair* pPair{ find(key) };
		if (pPair)return pPair->second;
		throw 0;
	}
	bool insert(const KeyValuePair& pair)
	{
		if (find(pair.first))
			return false;
		m_data.push_back(pair);
		return true;
	}
	bool insert(KeyValuePair&&pair)
	{
		if (find(pair.first))
			return false;
		m_data.push_back(std::move(pair));
		return true;
	}

	const_iterator begin()const { return m_data.begin(); }
	iterator begin() { return m_data.begin(); }

	const_iterator end()const { return m_data.end(); }
	iterator end() { return m_data.end(); }

	size_t size()const { return m_data.size(); }

	template<class Ch, class Tr, class = std::enable_if_t<is_streamable_v<std::basic_ostream<Ch, Tr>, Key>&&is_streamable_v<std::basic_ostream<Ch, Tr>, Value>>>
	friend auto& operator<<(std::basic_ostream<Ch, Tr>& os, const SeqMap& map)
	{
		os << '{';
		size_t len = map.size();

		if (len)
		{
			os << '{' << map.m_data[0].first << ',' << map.m_data[0].second << '}';
			for (size_t i{ 1 }; i != len; ++i)
				os << ',' << '{' << map.m_data[i].first << ',' << map.m_data[i].second << '}';
		}

		os << '}';
		return os;
	}
};

std::wstring charPtr2Wstring(const char* ptr)
{
	size_t size{ strlen(ptr) };
	std::wstring result(size, L'\0');
	for (size_t index{}; index != size; ++index)
		result[index] = static_cast<wchar_t>(ptr[index]);
	return result;
}

std::wstring string2Wstring(const std::string& str)
{
	size_t size{ str.size() };
	std::wstring result(size, 0);
	for (size_t index{}; index != size; ++index)
		result[index] = static_cast<wchar_t>(str[index]);
	return result;
}

namespace Json
{
	struct Value;

	using Undefined = std::monostate;
	using Null = std::nullptr_t;
	using Bool = bool;
	using Number = long double;
	using String = std::wstring;
	using List = std::vector<Value>;
	using Object = SeqMap<String, Value>;

	struct Value
	{
		using data_t = std::variant<Undefined, Null, Bool, Number, String, List, Object>;
	private:
		data_t m_data;
	public:

		Value() :m_data{ Null{} } {}

		Value(Undefined) :m_data{ Undefined{} } {}
		Value(Null) :m_data{ Null{} } {}

		Value(Bool v) :m_data{ v } {}

		Value(float v) :m_data{ static_cast<Number>(v) } {}
		Value(double v) :m_data{ static_cast<Number>(v) } {}
		Value(long double v) :m_data{ static_cast<Number>(v) } {}

		Value(uint8_t v) :m_data{ static_cast<Number>(v) } {}
		Value(uint16_t v) :m_data{ static_cast<Number>(v) } {}
		Value(uint32_t v) :m_data{ static_cast<Number>(v) } {}
		Value(uint64_t v) :m_data{ static_cast<Number>(v) } {}

		Value(int8_t v) :m_data{ static_cast<Number>(v) } {}
		Value(int16_t v) :m_data{ static_cast<Number>(v) } {}
		Value(int32_t v) :m_data{ static_cast<Number>(v) } {}
		Value(int64_t v) :m_data{ static_cast<Number>(v) } {}

		Value(std::wstring v) :m_data{ std::move(v) } {}
		Value(std::string v) :m_data{ string2Wstring(v) } {}
		Value(const char* v) :m_data{ charPtr2Wstring(v) } {}
		Value(const wchar_t* v) :m_data{ std::wstring{v} } {}

		Value(std::initializer_list<Value> list) :m_data{ list } {}

		Value(const List& list) :m_data{ list } {}
		Value(const Object& map) :m_data{ map } {}

		Value(List&& list) :m_data{ std::move(list) } {}
		Value(Object&& map) :m_data{ std::move(map) } {}

		template<class T>
		Value(const std::vector<T>& list)
		{
			List result;
			result.reserve(list.size());
			for (const T& ele : list)
				result.emplace_back(ele);
			m_data = std::move(result);
		}

		bool isUndefined()const noexcept { return std::holds_alternative<Undefined>(m_data); }
		bool isNull()const noexcept { return std::holds_alternative<Null>(m_data); }
		bool isBool()const noexcept { return std::holds_alternative<Bool>(m_data); }
		bool isNumber()const noexcept { return std::holds_alternative<Number>(m_data); }
		bool isString()const noexcept { return std::holds_alternative<String>(m_data); }
		bool isList()const noexcept { return std::holds_alternative<List>(m_data); }
		bool isObject()const noexcept { return std::holds_alternative<Object>(m_data); }

		const Bool& asBool() const { return std::get<Bool>(m_data); }		 Bool& asBool() { return std::get<Bool>(m_data); }
		const Number& asNumber() const { return std::get<Number>(m_data); }	 Number& asNumber() { return std::get<Number>(m_data); }
		const String& asString() const { return std::get<String>(m_data); } String& asString() { return std::get<String>(m_data); }
		const List& asList() const { return std::get<List>(m_data); }		 List& asList() { return std::get<List>(m_data); }
		const Object& asObject() const { return std::get<Object>(m_data); } Object& asObject() { return std::get<Object>(m_data); }

		void toWstreamUndefined(std::wostream& os)const { os << WXS(L"Undefined"); }
		void toWstreamNull(std::wostream& os)const { os << WXS(L"Null"); }
		void toWstreamBool(std::wostream& os)const { if (asBool())os << WXS(L"True"); else os << WXS(L"False"); }

		static void toWstreamStringEx(std::wostream& os, const String& str)
		{
			os << L'"';
			for (const wchar_t& c : str)
			{
				if (c == L'"')os << WXS(L"\\\"");
				else if (c == L'\\') os << WXS(L"\\\\");
				else if (c == L'\b') os << WXS(L"\\b");
				else if (c == L'\f') os << WXS(L"\\f");
				else if (c == L'\n') os << WXS(L"\\n");
				else if (c == L'\r') os << WXS(L"\\r");
				else if (c == L'\t') os << WXS(L"\\t");
				else if (c < (wchar_t)0x20)
				{
					os << WXS(L"\\u");
					os << std::hex << std::uppercase << std::setw(4) << std::setfill(L'0');
					os << (uint16_t)c;
				}
				else os << c;
			}
			os << L'"';
		}

		void toWstreamString(std::wostream& os)const
		{
			toWstreamStringEx(os, asString());
		}

		template<bool Beautify, int numberBase, class IndentT, class NewLineT>
		void toWstreamList(std::wostream& os, int depth)const
		{
			os << L'[';
			const List& list{ asList() };
			size_t length{ list.size() };
			for (size_t i{}; i < length; ++i)
			{
				if constexpr (Beautify)
					os << NewLineT{};
				list[i].toWstream<Beautify, numberBase, IndentT, NewLineT>(os, depth + 1);
				if (i != length - 1)
					os << L',';
			}
			if constexpr (Beautify)
				if (list.size())
				{
					os << NewLineT{};
					for (int i{}; i < depth; ++i)
						os << IndentT{};
				}
			os << L']';
		}

		template<bool Beautify, int numberBase, class IndentT, class NewLineT>
		void toWstreamObject(std::wostream& os, int depth)const
		{
			os << L'{';
			const Object& dict{ asObject() };
			size_t length{ dict.size() };
			size_t count{ 0 };
			for (const auto& pair : dict)
			{
				if constexpr (Beautify)
				{
					os << NewLineT{};
					for (int i{}; i < depth + 1; ++i)
						os << IndentT{};
				}
				toWstreamStringEx(os, pair.first);
				os << L':';
				if constexpr (Beautify)
				{
					os << L' ';
					if (pair.second.isObject() || pair.second.isList())
					{
						os << NewLineT{};
						for (int i{}; i < depth + 1; ++i)
							os << IndentT{};
					}
				}
				pair.second.toWstream<Beautify, numberBase, IndentT, NewLineT, true>(os, depth + 1);
				if (count != length - 1)
				{
					os << L',';
					++count;
				}
			}
			if constexpr (Beautify)
				if (dict.size())
				{
					os << NewLineT{};
					for (int i{}; i < depth; ++i)
						os << IndentT{};
				}
			os << L'}';
		}

		template<int numberBase>
		void toWstreamNumber(std::wostream& os)const
		{
			static_assert(numberBase == 2 || numberBase == 10 || numberBase == 16, "Only Base2, Base10, Base16 are supported");

			Number value = asNumber();

			if (std::isnan(value))os << WXS(L"NaN");
			else if (value == std::numeric_limits<Number>::infinity())os << WXS(L"Infinity");
			else if (value == -std::numeric_limits<Number>::infinity())os << WXS(L"-Infinity");
			else
			{
				if (numeric_details::isInt(value) && (std::abs(value) < std::numeric_limits<int64_t>::max()))
				{
					std::vector<wchar_t> vecNum;
					if (value < 0)os << L'-';
					if constexpr (numberBase == 2)os << L"0b";
					if constexpr (numberBase == 16)os << L"0x";
					int64_t intVal{ std::abs(int64_t(value)) };

					if (intVal == 0)
						os << L'0';
					else
					{
						while (intVal > 0)
						{
							if constexpr (numberBase == 10)
							{
								switch (intVal % 10)
								{
									case 0: vecNum.push_back(L'0'); break;
									case 1: vecNum.push_back(L'1'); break;
									case 2: vecNum.push_back(L'2'); break;
									case 3: vecNum.push_back(L'3'); break;
									case 4: vecNum.push_back(L'4'); break;
									case 5: vecNum.push_back(L'5'); break;
									case 6: vecNum.push_back(L'6'); break;
									case 7: vecNum.push_back(L'7'); break;
									case 8: vecNum.push_back(L'8'); break;
									case 9: vecNum.push_back(L'9'); break;
								}
							}
							else if constexpr (numberBase == 16)
							{
								switch (intVal % 16)
								{
									case 0: vecNum.push_back(L'0'); break;
									case 1: vecNum.push_back(L'1'); break;
									case 2: vecNum.push_back(L'2'); break;
									case 3: vecNum.push_back(L'3'); break;
									case 4: vecNum.push_back(L'4'); break;
									case 5: vecNum.push_back(L'5'); break;
									case 6: vecNum.push_back(L'6'); break;
									case 7: vecNum.push_back(L'7'); break;
									case 8: vecNum.push_back(L'8'); break;
									case 9: vecNum.push_back(L'9'); break;
									case 10: vecNum.push_back(L'A'); break;
									case 11: vecNum.push_back(L'B'); break;
									case 12: vecNum.push_back(L'C'); break;
									case 13: vecNum.push_back(L'D'); break;
									case 14: vecNum.push_back(L'E'); break;
									case 15: vecNum.push_back(L'F'); break;
								}
							}
							else if constexpr (numberBase == 2)
							{
								switch (intVal % 2)
								{
									case 0:vecNum.push_back(L'0'); break;
									case 1:vecNum.push_back(L'1'); break;
								}
							}
							intVal /= numberBase;
						}
						for (auto iter{ vecNum.rbegin() }; iter != vecNum.rend(); ++iter)
							os << *iter;
					}
				}
				else
					os << value;
			}
		}

		template<bool Beautify = 1, int numberBase = 10, class IndentT = ct::String<wchar_t, L'\t'>, class NewLineT = ct::String<wchar_t, L'\n'>, bool Suppress = 0>
		void toWstream(std::wostream& os, int depth = 0)const
		{
			if constexpr (Beautify && !Suppress)
				for (int i{}; i < depth; ++i)
					os << IndentT{};
			if (isUndefined())return toWstreamUndefined(os);
			else if (isNull())return toWstreamNull(os);
			else if (isBool())return toWstreamBool(os);
			else if (isNumber())return toWstreamNumber<numberBase>(os);
			else if (isString())return toWstreamString(os);
			else if (isList())return toWstreamList<Beautify, numberBase, IndentT, NewLineT>(os, depth);
			else if (isObject())return toWstreamObject<Beautify, numberBase, IndentT, NewLineT>(os, depth);
			else os << WXS(L"Error");
		}

		template<bool Beautify = 1, int numberBase = 10, class IndentT = ct::String<wchar_t, L'\t'>, class NewLineT = ct::String<wchar_t, L'\n'>>
		std::wstring toWstring()const
		{
			std::wostringstream ss;
			toWstream<Beautify, numberBase, IndentT, NewLineT>(ss);
			return ss.str();
		}

		template<bool Beautify = 1, int numberBase = 10, class IndentT = ct::String<wchar_t, L'\t'>, class NewLineT = ct::String<wchar_t, L'\n'>>
		std::string toString()const
		{
			std::ostringstream ss;
			for (const wchar_t& c : toWstring<Beautify, numberBase, IndentT, NewLineT>())
			{
				if (c > L'\xff')
					ss << "\\u" << std::hex << std::uppercase << std::setw(4) << std::setfill('0') << (uint16_t)c;
				else if (c >= (wchar_t)0x7f) //(wchar_t)0x7f = DEL
					ss << "\\x" << std::hex << std::uppercase << std::setw(2) << std::setfill('0') << (uint16_t)c;
				else
					ss << char(c);
			}
			return ss.str();
		}

		friend std::wostream& operator<<(std::wostream& os, const Value& value) { value.toWstream(os); return os; }

		template<class char_t>
		static void toNextToken(const char_t** ppStr)
		{
			static_assert(std::is_same_v<char, char_t> || std::is_same_v<wchar_t, char_t>);

			for (;;)
			{
				switch (**ppStr)
				{
					case char_t{ ' ' }:case char_t{ '\t' }:case char_t{ '\r' }:case char_t{ '\n' }:
						++*ppStr;
						break;
					case char_t{ '/' }:
					{
						const wchar_t c = (*ppStr)[1];
						if (c == char_t{ '/' })
						{
							++*ppStr;
							for (;;)
							{
								const wchar_t cc = **ppStr;
								if (cc == char_t{ '\n' })
								{
									++*ppStr;
									break;
								}
								else if (cc == char_t{ '\0' })
									return;
								else ++*ppStr;
							}
							break;
						}
						else if (c == char_t{ '*' })
						{
							++*ppStr;
							for (;;)
							{
								const wchar_t cc = **ppStr;
								if (cc == char_t{ '*' } && (*ppStr)[1] == char_t{ '/' })
								{
									*ppStr = *ppStr + 2;
									break;
								}
								else if (cc == char_t{ '\0' })
									return;
								else ++*ppStr;
							}
							break;
						}
						else
							return;//unexpected symbol at (*ppStr)[0] and (*ppStr)[1]
						break;//should never reach this line
					}
					default:return;
				}
			}
		}

		template<class char_t>bool parse(const char_t* pStr, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			toNextToken(&pStr);
			switch (*pStr)
			{
				case '+':case '-':
				case '.':
				case 'i':case 'I':
				case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':
					return parseNumber(pStr, pErrMsg, ppNext);
				case '`':case '"':case '\'':
					return parseString(pStr, pErrMsg, ppNext);
				case '[':
					return parseList(pStr, pErrMsg, ppNext);
				case '{':
					return parseObject(pStr, pErrMsg, ppNext);
				case 'T':case 't':case 'F':case 'f':
					return parseBool(pStr, pErrMsg, ppNext);
				case 'N':case 'n':
					return parseNullOrNan(pStr, pErrMsg, ppNext);
				case 'U':case 'u':
					return parseUndefined(pStr, pErrMsg, ppNext);
				default:
					if (pErrMsg)
					{
						std::wostringstream ssErr;
						ssErr << WXS(L"Unexpected symbol:") << *pStr << ';';
						*pErrMsg += ssErr.str();
					}
					return false;
			}
		}
		template<class char_t>bool parseUndefined(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			toNextToken(&pStart);
			if ((pStart[0] == 'U' || pStart[0] == 'u') &&
				(pStart[1] == 'N' || pStart[1] == 'n') &&
				(pStart[2] == 'D' || pStart[2] == 'd') &&
				(pStart[3] == 'E' || pStart[3] == 'e') &&
				(pStart[4] == 'F' || pStart[4] == 'f') &&
				(pStart[5] == 'I' || pStart[5] == 'i') &&
				(pStart[6] == 'N' || pStart[6] == 'n') &&
				(pStart[7] == 'E' || pStart[7] == 'e') &&
				(pStart[8] == 'D' || pStart[8] == 'd'))
			{
				if (ppNext)*ppNext = pStart + 9;
				operator=(Undefined{});
				return true;
			}
			else
			{
				if (pErrMsg)*pErrMsg += WXS(L"Fail to parse undefined;").str();
				return false;
			}
		}
		template<class char_t>bool parseNullOrNan(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			toNextToken(&pStart);
			if (pStart[0] == 'n' || pStart[0] == 'N')
			{
				if ((pStart[1] == 'u' || pStart[1] == 'U') &&
					(pStart[2] == 'l' || pStart[2] == 'L') &&
					(pStart[3] == 'l' || pStart[3] == 'L'))
				{
					if (ppNext)*ppNext = pStart + 4;
					operator=(Null{});
					return true;
				}
				else
					if ((pStart[1] == 'a' || pStart[1] == 'A') &&
						(pStart[2] == 'n' || pStart[2] == 'N'))
					{
						if (ppNext)*ppNext = pStart + 3;
						operator=(std::numeric_limits<Number>::quiet_NaN());
						return true;
					}
			}

			if (pErrMsg)*pErrMsg += WXS(L"Fail to parse null or nan;").str();
			return false;
		}
		template<class char_t>bool parseBool(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			toNextToken(&pStart);
			switch (*pStart)
			{
				case 'T':case 't':
				{
					if ((pStart[1] == 'R' || pStart[1] == 'r') &&
						(pStart[2] == 'U' || pStart[2] == 'u') &&
						(pStart[3] == 'E' || pStart[3] == 'e'))
					{
						operator=(true);
						if (ppNext)*ppNext = pStart + 4;
						return true;
					}
					else
					{
						if (pErrMsg)*pErrMsg += WXS(L"Fail to parse bool(True);").str();
						return false;
					}
				}
				case 'F':case 'f':
				{
					if ((pStart[1] == 'A' || pStart[1] == 'a') &&
						(pStart[2] == 'L' || pStart[2] == 'l') &&
						(pStart[3] == 'S' || pStart[3] == 's') &&
						(pStart[4] == 'E' || pStart[4] == 'e'))
					{
						operator=(false);
						if (ppNext)*ppNext = pStart + 5;
						return true;
					}
					else
					{
						if (pErrMsg)*pErrMsg += WXS(L"Fail to parse bool(False);").str();
						return false;
					}
				}
				default:
				{
					if (pErrMsg)
					{
						std::wostringstream ssErr;
						ssErr << WXS(L"Fail to parse bool: invalid charecter") << *pStart << ';';
						*pErrMsg += ssErr.str();
					}

					return false;
				}
			}
		}
		template<class char_t>bool parseNumber(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			toNextToken(&pStart);
			switch (*pStart)
			{
				case '-':
				{
					if (parseNumber(pStart + 1, pErrMsg, ppNext))
					{
						asNumber() = -asNumber();
						return true;
					}
					else return false;
				}
				case '+':
				{
					return parseNumber(pStart + 1, pErrMsg, ppNext);
				}
				case '.':case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':
				{
					if (pStart[0] == '0')
					{
						if (pStart[1] == 'x' || pStart[1] == 'X')
							return parseHexNumber(pStart, pErrMsg, ppNext);
						else if (pStart[1] == 'b' || pStart[1] == 'B')
							return parseBinaryNumber(pStart, pErrMsg, ppNext);
					}
					return parseDecNumber(pStart, pErrMsg, ppNext);
				}
				case 'i':case 'I':
				{
					if ((pStart[1] == 'n' || pStart[1] == 'N') &&
						(pStart[2] == 'f' || pStart[2] == 'F') &&
						(pStart[3] == 'i' || pStart[3] == 'I') &&
						(pStart[4] == 'n' || pStart[4] == 'N') &&
						(pStart[5] == 'i' || pStart[5] == 'I') &&
						(pStart[6] == 't' || pStart[6] == 'T') &&
						(pStart[7] == 'y' || pStart[7] == 'Y')
						)
					{
						operator=(std::numeric_limits<Number>::infinity());
						if (ppNext)*ppNext = pStart + 8;
						return true;
					}
					else
					{
						if (pErrMsg)*pErrMsg += WXS(L"Fail to parse infinity;").str();
						return false;
					}
				}
				default:
				{
					if (pErrMsg)
					{
						std::wostringstream ssErr;
						ssErr << WXS(L"Unexpected symbol:") << *pStart << ';';
						*pErrMsg += ssErr.str();
					}
					return false;
				}
			}
		}
		template<class char_t>bool parseString(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			toNextToken(&pStart);
			char_t terminator = *pStart;
			if (terminator != '`'&&terminator != '"'&&terminator != '\'')
			{
				if (pErrMsg)
				{
					std::wostringstream ssErr;
					ssErr << WXS(L"Fail to parse String, invalid terminator:'") << terminator << WXS(L"';");
					*pErrMsg += ssErr.str();
				}
				return false;
			}
			std::wostringstream ssResult;
			for (const char_t*pChar = pStart + 1;;)
			{
				if (*pChar == '\\')
				{
					++pChar;
					switch (*pChar)
					{
						case '"':case '`':case '\'':case '\\':	ssResult << *pChar;	break;
						case '0':								ssResult << '\0';	break; //bug
						case 'b':								ssResult << '\b';	break;
						case 'f':								ssResult << '\f';	break;
						case 'n':								ssResult << '\n';	break;
						case 'r':								ssResult << '\r';	break;
						case 't':								ssResult << '\t';	break;
						case 'x':
						{
							std::array<uint8_t, 2> buffer;
							for (int i{}; i != 2; ++i)
							{
								++pChar;
								switch (*pChar)
								{
									case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':	buffer[i] = static_cast<uint8_t>(*pChar - '0');		break;
									case 'A':case 'B':case 'C':case 'D':case 'E':case 'F':										buffer[i] = static_cast<uint8_t>(*pChar - 'A' + 10); break;
									case 'a':case 'b':case 'c':case 'd':case 'e':case 'f':										buffer[i] = static_cast<uint8_t>(*pChar - 'a' + 10); break;
									default:
										if (pErrMsg)
										{
											std::wostringstream ssErr;
											ssErr << WXS(L"Unexpected symbol:") << *pChar << ';';
											*pErrMsg += ssErr.str();
										}
										return false;
								}
							}
							ssResult << char_t(buffer[0] * 16 + buffer[1]);
							break;
						}
						case 'u':
						{
							std::array<uint8_t, 4> buffer;
							for (int i{ }; i < 4; ++i)
							{
								++pChar;
								switch (*pChar)
								{
									case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':	buffer[i] = static_cast<uint8_t>(*pChar - '0');		break;
									case 'A':case 'B':case 'C':case 'D':case 'E':case 'F':										buffer[i] = static_cast<uint8_t>(*pChar - 'A' + 10); break;
									case 'a':case 'b':case 'c':case 'd':case 'e':case 'f':										buffer[i] = static_cast<uint8_t>(*pChar - 'a' + 10); break;
									default:
										if (pErrMsg)
										{
											std::wostringstream ssErr;
											ssErr << WXS(L"Unexpected symbol:") << *pChar << ';';
											*pErrMsg += ssErr.str();
										}
										return false;
								}
							}
							ssResult << char_t(buffer[0] * (16 * 16 * 16) + buffer[1] * (16 * 16) + buffer[2] * 16 + buffer[3]);
							break;
						}
						default:
						{
							if (pErrMsg)
							{
								std::wostringstream ssErr;
								ssErr << WXS(L"Unexpected symbol:") << *pChar << ';';
								*pErrMsg += ssErr.str();
							}
							return false;
						}
					}
					++pChar;
				}
				else if (*pChar == terminator)
				{
					if (ppNext)*ppNext = pChar + 1;
					operator= (ssResult.str());
					return true;
				}
				else if (*pChar == '\0')
				{
					if (pErrMsg)*pErrMsg += WXS(L"Unexpected end of input string;").str();
					return false;
				}
				else
				{
					ssResult << *pChar;
					++pChar;
				}
			}
		}
		template<class char_t>bool parseList(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			static_assert(std::is_same_v<char, char_t> || std::is_same_v<wchar_t, char_t>);
			toNextToken(&pStart);
			if (*pStart != '[')
			{
				if (pErrMsg)
				{
					std::wostringstream ssErr;
					ssErr << WXS(L"Fail to parse List, Unexpected symbol:") << *pStart << ';';
					*pErrMsg += ssErr.str();
				}
				return false;
			}
			++pStart;
			toNextToken(&pStart);
			List result{};
			if (*pStart == ']')
			{
				if (ppNext)*ppNext = pStart + 1;
				operator=(std::move(result));
				return true;
			}
			for (;;)
			{
				if (*pStart == ']')
				{
					result.emplace_back(Undefined{});
					if (ppNext)*ppNext = pStart + 1;
					operator=(std::move(result));
					return true;
				}
				else if (*pStart == ',')
				{
					result.emplace_back(Undefined{});
					++pStart;
					toNextToken(&pStart);
					while (*pStart == ',')
					{
						result.emplace_back(Undefined{});
						++pStart;
						toNextToken(&pStart);
					}
				}
				else
				{
					const char_t *pNext{ pStart };
					Value newElement{};
					if (!newElement.parse(pStart, pErrMsg, &pNext))
					{
						if (pErrMsg)*pErrMsg += WXS(L"Fail to parse list;").str();
						return false;
					}
					result.push_back(std::move(newElement));
					pStart = pNext;
					toNextToken(&pStart);
					switch (*pStart)
					{
						case ']':
						{
							if (ppNext)*ppNext = pStart + 1;
							operator=(std::move(result));
							return true;
						}
						case ',':
						{
							++pStart;
							toNextToken(&pStart);
							continue;
						}
						default:
						{
							if (pErrMsg)
							{
								std::wostringstream ssErr;
								ssErr << WXS(L"Unexpected symbol:") << *pStart << ';';
								*pErrMsg += ssErr.str();
							}
							return false;
						}
					}
				}
			}
		}
		template<class char_t>bool parseKey(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			static_assert(std::is_same_v<char, char_t> || std::is_same_v<wchar_t, char_t>);
			toNextToken(&pStart);
			switch (*pStart)
			{
				case '`':case '"':case '\'':
					return parseString(pStart, pErrMsg, ppNext);
				case '}':
				{
					if (pErrMsg)
					{
						std::wostringstream ssErr;
						ssErr << WXS(L"Fail to parse Key, Unexpected symbol:") << *pStart << ';';
						*pErrMsg += ssErr.str();
					}
					return false;
				}
				default:
				{
					std::wostringstream ssResult;
					const char_t*pEnd = pStart;
					const char_t*pChar = pStart;
					for (;;)
					{
						switch (*pChar)
						{
							case ':':
							{
								if (ppNext)*ppNext = pChar;
								for (const char_t*pC = pStart; pC != pEnd; ++pC)
									ssResult << *pC;
								operator=(ssResult.str());
								return true;
							}
							case '\0':
							{
								if (pErrMsg)*pErrMsg += WXS(L"Unexpected end of input string;").str();
								return false;
							}
							case '\\'://case '{':case '}':case '[':case ']':
							{
								if (pErrMsg)
								{
									std::wostringstream ssErr;
									ssErr << WXS(L"Unexpected symbol") << *pChar << ';';
									*pErrMsg += ssErr.str();
								}
								return false;
							}
							case ' ':case '\t':case '\r':case '\n':
							{
								++pChar;
								break;
							}
							default:
							{
								//ssResult << *pChar;
								++pChar;
								pEnd = pChar;
							}
						}
					}
				}
			}
		}
		template<class char_t>bool parseObject(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			static_assert(std::is_same_v<char, char_t> || std::is_same_v<wchar_t, char_t>);
			toNextToken(&pStart);
			if (*pStart != '{')
			{
				if (pErrMsg)
				{
					std::wostringstream ssErr;
					ssErr << WXS(L"Fail to parse Object, Unexpected symbol:") << *pStart << ';';
					*pErrMsg += ssErr.str();
				}
				return false;
			}
			++pStart;
			toNextToken(&pStart);
			Object result{};
			if (*pStart == '}')
			{
				if (ppNext)*ppNext = pStart + 1;
				operator=(std::move(result));
				return true;
			}

			for (;;)
			{
				const char_t*pNext{ pStart };
				Value newKeyObj{};
				if (!newKeyObj.parseKey(pStart, pErrMsg, &pNext))
				{
					if (pErrMsg)*pErrMsg += WXS(L"Fail to parse Key;").str();
					return false;
				}
				const String& newKeyStr{ newKeyObj.asString() };
				if (result.find(newKeyStr))
				{
					if (pErrMsg)
					{
						std::wostringstream ssErr;
						ssErr << WXS(L"Repetitive Key:\"") << newKeyStr << WXS(L"\";");
						*pErrMsg += ssErr.str();
					}
					return false;
				}
				pStart = pNext;
				toNextToken(&pStart);
				if (*pStart != ':')
				{
					if (pErrMsg)
					{
						std::wostringstream ssErr;
						ssErr << WXS(L"Unexpected symbol:") << *pStart << WXS(L", Expected:':';");
						*pErrMsg += ssErr.str();
					}
					return false;
				}
				++pStart;
				toNextToken(&pStart);
				Value newObjectValue{};
				if (!newObjectValue.parse(pStart, pErrMsg, &pNext))
				{
					if (pErrMsg)*pErrMsg += WXS(L"Fail to parse Object;").str();
					return false;
				}
				if (!result.insert({ std::move(newKeyStr),std::move(newObjectValue) }))
				{
					if (pErrMsg)*pErrMsg += WXS(L"Key Value pair insertion failed;").str();
					return false;
				}
				pStart = pNext;
				toNextToken(&pStart);
				switch (*pStart)
				{
					case '}':
					{
						if (ppNext)*ppNext = pStart + 1;
						operator=(std::move(result));
						return true;
					}
					case ',':
					{
						++pStart;
						toNextToken(&pStart);
						continue;
					}
					default:
					{
						if (pErrMsg)
						{
							std::wostringstream ssErr;
							ssErr << WXS(L"Unexpected symbol:") << *pStart << ';';
							*pErrMsg += ssErr.str();
						}
						return false;
					}
				}
			}
		}
		template<class char_t>bool parseDecNumber(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			static_assert(std::is_same_v<char, char_t> || std::is_same_v<wchar_t, char_t>);
			std::vector<int_fast8_t> vecInt;
			std::vector<int_fast8_t> vecFrac;
			std::vector<int_fast8_t>* pActiveVec{ &vecInt };

			const char_t* pChar = pStart;

			for (;;)
			{
				switch (*pChar)
				{
					case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':
					{
						pActiveVec->emplace_back(static_cast<int_fast8_t>(*pChar - '0'));
						++pChar;
						break;
					}
					case '.':
					{
						if (pActiveVec == &vecInt)
						{
							pActiveVec = &vecFrac;
							++pChar;
							break;
						}
						else
						{
							if (pErrMsg)*pErrMsg += WXS(L"Unexpected second '.',Fail to parse decimal number;").str();
							return false;
						}
					}
					//case 'E':case 'e':
					default:
					{
						Number DigitMult = 10;
						Number DigitMultRev = 1. / DigitMult;
						Number result = 0;
						Number multiplier{ 1 };

						for (auto iter{ vecInt.rbegin() }; iter != vecInt.rend(); ++iter)
						{
							result += multiplier * Number(*iter);
							multiplier *= DigitMult;
						}
						multiplier = DigitMultRev;
						for (size_t i{}; i < vecFrac.size(); ++i)
						{
							result += multiplier * Number(vecFrac[i]);
							multiplier *= DigitMultRev;
						}
						if (*pChar == 'e' || *pChar == 'E')
						{
							Value valueExp;
							if (valueExp.parseNumber(pChar + 1, pErrMsg, ppNext))
							{
								operator=(result * std::pow(10, valueExp.asNumber()));
								return true;
							}
							else
							{
								if (pErrMsg)*pErrMsg += WXS(L"Fail to parse exp part, fail to parse dec number;").str();
								return false;
							}
						}
						else
						{
							operator= (result);
							if (ppNext)*ppNext = pChar;
							return true;
						}
					}
				}
			}
		}
		template<class char_t>bool parseBinaryNumber(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			static_assert(std::is_same_v<char, char_t> || std::is_same_v<wchar_t, char_t>);
			toNextToken(&pStart);
			if (!(pStart[0] == '0' && ((pStart[1] == 'b') || (pStart[1] == 'B'))))
			{
				if (pErrMsg)
				{
					std::wostringstream ssErr;
					ssErr << WXS(L"Invalid binary number prefix;\"") << pStart[0] << pStart[1] << WXS(L"\";");
					*pErrMsg += ssErr.str();
				}
				return false;
			}

			std::vector<int_fast8_t> vecInt;
			std::vector<int_fast8_t> vecFrac;
			std::vector<int_fast8_t>* pActivePart = &vecInt;
			const char_t*pChar = pStart + 2;

			for (;;)
			{
				switch (*pChar)
				{
					case '0':case '1':
					{
						pActivePart->emplace_back(static_cast<int_fast8_t>(*pChar - '0'));
						++pChar;
						break;
					}
					case '.':
					{
						if (pActivePart == &vecInt)
						{
							pActivePart = &vecFrac;
							++pChar;
							break;
						}
						else
						{
							if (pErrMsg)*pErrMsg += WXS(L"Unexpected second '.',Fail to parse binary number;").str();
							return false;
						}
					}
					default:
					{
						Number DigitMult = 2;
						Number DigitMultRev = 1. / DigitMult;
						Number result = 0;
						Number multiplier{ 1 };
						for (auto iter{ vecInt.rbegin() }; iter != vecInt.rend(); ++iter)
						{
							result += multiplier * Number(*iter);
							multiplier *= DigitMult;
						}
						multiplier = DigitMultRev;
						for (size_t i{}; i < vecFrac.size(); ++i)
						{
							result += multiplier * Number(vecFrac[i]);
							multiplier *= DigitMultRev;
						}
						operator= (result);
						if (ppNext)*ppNext = pChar;
						return true;
					}
				}
			}
		}
		template<class char_t>bool parseHexNumber(const char_t*pStart, std::wstring* pErrMsg = nullptr, const char_t** ppNext = nullptr)
		{
			static_assert(std::is_same_v<char, char_t> || std::is_same_v<wchar_t, char_t>);

			toNextToken(&pStart);
			if (!(pStart[0] == '0' && ((pStart[1] == 'x') || (pStart[1] == 'X'))))
			{
				if (pErrMsg)
				{
					std::wostringstream ssErr;
					ssErr << WXS(L"Invalid hex number prefix;\"") << pStart[0] << pStart[1] << WXS(L"\";");
					*pErrMsg += ssErr.str();
				}
				return false;
			}

			std::vector<int_fast8_t> vecInt;
			std::vector<int_fast8_t> vecFrac;
			std::vector<int_fast8_t>* pActivePart = &vecInt;
			const char_t*pChar = pStart + 2;

			for (;;)
			{
				switch (*pChar)
				{
					case '0':case '1':case '2':case '3':case '4':case '5':case '6':case '7':case '8':case '9':
					{
						pActivePart->emplace_back(static_cast<int_fast8_t>(*pChar - '0'));
						++pChar;
						break;
					}
					case 'a':case 'b':case 'c':case 'd':case 'e':case 'f':
					{
						pActivePart->emplace_back(static_cast<int_fast8_t>(*pChar - 'a' + 10));
						++pChar;
						break;
					}
					case 'A':case 'B':case 'C':case 'D':case 'E':case 'F':
					{
						pActivePart->emplace_back(static_cast<int_fast8_t>(*pChar - 'A' + 10));
						++pChar;
						break;
					}
					case '.':
					{
						if (pActivePart == &vecInt)
						{
							pActivePart = &vecFrac;
							++pChar;
							break;
						}
						else
						{
							if (pErrMsg)*pErrMsg += WXS(L"Unexpected second '.',Fail to parse hex number;").str();
							return false;
						}
					}
					default:
					{
						Number DigitMult = 16;
						Number DigitMultRev = 1. / DigitMult;
						Number result = 0;
						Number multiplier{ 1 };
						for (auto iter{ vecInt.rbegin() }; iter != vecInt.rend(); ++iter)
						{
							result += multiplier * Number(*iter);
							multiplier *= DigitMult;
						}
						multiplier = DigitMultRev;
						for (size_t i{}; i < vecFrac.size(); ++i)
						{
							result += multiplier * Number(vecFrac[i]);
							multiplier *= DigitMultRev;
						}
						operator=(result);
						if (ppNext)*ppNext = pChar;
						return true;
					}
				}
			}
		}
	};

	Value parse(const char* str, std::wstring* pError = nullptr) { Value result; result.parse(str, pError); return result; }
	Value parse(const wchar_t* str, std::wstring* pError = nullptr) { Value result; result.parse(str, pError); return result; }
	Value parse(const std::string& str, std::wstring* pError = nullptr) { return parse(str.c_str(), pError); }
	Value parse(const std::wstring& str, std::wstring* pError = nullptr) { return parse(str.c_str(), pError); }
}

