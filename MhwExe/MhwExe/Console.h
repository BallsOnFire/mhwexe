#pragma once

#include <WinSock2.h>
#include <fcntl.h>
#include <io.h>
#include <string>
#include "writeln.h"
template<class T>
struct Vector2
{
	T x{}, y{};
	template<class U>Vector2 operator/(U v) const noexcept { return { x / v,y / v }; }
};

namespace Windows
{
	int getFirstScreenWidth() { return GetSystemMetrics(SM_CXSCREEN); }
	int getFirstScreenHeight() { return GetSystemMetrics(SM_CYSCREEN); }
	Vector2<int> getFirstScreenSize() { return { getFirstScreenWidth(),getFirstScreenHeight() }; }

	struct Console
	{
	private:
		HWND m_hwnd;
		HANDLE m_std_output_handle;
		FILE* pCout, *pCerr, *pCin;
	public:
		Console(const Vector2<int>& position = Windows::getFirstScreenSize() / 4, const Vector2<int>& size = Windows::getFirstScreenSize() / 2, const wchar_t* title = nullptr, float alpha = 0.9f, bool enableUnicode = true, bool showCursor = false)
		{
			AllocConsole();
			freopen_s(&pCout, "CONOUT$", "w", stdout);
			freopen_s(&pCerr, "CONOUT$", "w", stderr);
			freopen_s(&pCin, "CONIN$", "r", stdin);
			std::ios_base::sync_with_stdio(false);
			m_hwnd = GetConsoleWindow();
			m_std_output_handle = GetStdHandle(STD_OUTPUT_HANDLE);

			setUnicode(enableUnicode);
			setCursor(showCursor);
			setAlpha(alpha);
			setTitle(title);
			setPos(position, size);
		}

		~Console()
		{
			if (pCout)fclose(pCout);
			if (pCerr)fclose(pCerr);
			if (pCin)fclose(pCin);
			FreeConsole();
		}

		void setTitle(const wchar_t* title)
		{
			if (title)
				SetConsoleTitleW(title);
		}

		void setUnicode(bool enable)
		{
			if (enable)
			{
				SetConsoleCP(CP_UTF8);
				SetConsoleOutputCP(CP_UTF8);
				_setmode(_fileno(stdout), _O_U8TEXT);
				//_setmode(_fileno(stdin), _O_U8TEXT);
				_setmode(_fileno(stderr), _O_U8TEXT);
			}
		}

		void setCursor(bool enable)
		{
			if (!enable)
			{
				CONSOLE_CURSOR_INFO info{ 1,0 };
				SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &info);
			}
		}

		void setAlpha(float alpha)//0.f-1.f
		{
			if (!SetWindowLongPtrW(m_hwnd, GWL_EXSTYLE, GetWindowLongPtrW(m_hwnd, GWL_EXSTYLE) | WS_EX_LAYERED))
				throw 0;
			if (!SetLayeredWindowAttributes(m_hwnd, 0, BYTE(alpha * 255.f), LWA_ALPHA))
				throw 0;
		}

		void setPos(const Vector2<int>& position, const Vector2<int>& size)
		{
			SetWindowPos(m_hwnd, 0, position.x, position.y, size.x, size.y, SWP_NOSIZE | SWP_NOZORDER);
			SetWindowPos(m_hwnd, 0, position.x, position.y, size.x, size.y, SWP_NOZORDER);
		}

		void setCursorPos(const Vector2<short>& pos)const
		{
			SetConsoleCursorPosition(m_std_output_handle, reinterpret_cast<const COORD&>(pos));
		}

		struct Info
		{
			CONSOLE_SCREEN_BUFFER_INFO m_data;
			Info(HANDLE std_output_handle) { GetConsoleScreenBufferInfo(std_output_handle, &m_data); }

			const Vector2<short>& cursorPos()const
			{
				return reinterpret_cast<const Vector2<short>&>(m_data.dwCursorPosition);
			}
		};

		Info getInfo()const
		{
			return { m_std_output_handle };
		}

		Vector2<short> getCursorPos()const
		{
			return getInfo().cursorPos();
		}

	private:
		Console(const Console&) = delete;
		Console(Console&&) = delete;
		Console& operator=(const Console&) = delete;
		Console& operator=(Console&&) = delete;
	};
}