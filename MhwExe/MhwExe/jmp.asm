.data
	extern AddrDllMain : qword
	extern AddrXInputGetCapabilities : qword
	extern AddrXInputGetDSoundAudioDeviceGuids : qword
	extern AddrXInputGetState : qword
	extern AddrXInputSetState : qword
.code
	JmpDllMain proc
		jmp qword ptr [AddrDllMain]
	JmpDllMain endp

	JmpXInputGetCapabilities proc
		jmp qword ptr [AddrXInputGetCapabilities]
	JmpXInputGetCapabilities endp

	JmpXInputGetDSoundAudioDeviceGuids proc
		jmp qword ptr [AddrXInputGetDSoundAudioDeviceGuids]
	JmpXInputGetDSoundAudioDeviceGuids endp

	JmpXInputGetState proc
		jmp qword ptr [AddrXInputGetState]
	JmpXInputGetState endp

	JmpXInputSetState proc
		jmp qword ptr [AddrXInputSetState]
	JmpXInputSetState endp
end