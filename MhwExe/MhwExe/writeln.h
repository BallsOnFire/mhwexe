#pragma once

#include <type_traits>
#include <stdio.h>
#include <tuple>
#include <array>
#include <utility>
#include <sstream>
#include <string>
#include <windows.h>

namespace writeln_details
{
	template<size_t N, class T, class... types>struct nth_type { using type = typename nth_type<N - 1, types...>::type; };
	template<class T, class... types>struct nth_type<0, T, types...> { using type = T; };
	template<size_t N, class...Args>using nth_type_t = typename nth_type<N, Args...>::type;

	template<class T, class U>
	constexpr decltype(auto) forward_like(U&& u) noexcept
	{
		if constexpr (std::is_lvalue_reference_v<T>)
			return std::forward<U>(u);
		else
			return std::move(u);
	}

	template<class, class, class = void>struct is_streamable : std::false_type {};
	template<class Stream, class T>struct is_streamable<Stream, T, std::void_t<decltype(std::declval<Stream&>() << std::declval<T>())>> : std::true_type {};
	template<class Stream, class T>constexpr bool is_streamable_v = is_streamable<Stream, T>::value;

	template<class T>struct is_integer_sequence : std::false_type {};
	template<class T, size_t...I>struct is_integer_sequence<std::integer_sequence<T, I...>> : std::true_type {};
	template<class T>constexpr bool is_integer_sequence_v = is_integer_sequence<std::decay_t<T>>::value;

	template<class T>struct is_pair : std::false_type {};
	template<class ... Types>struct is_pair<std::pair<Types...>> : std::true_type {};
	template<class T>constexpr bool is_pair_v = is_pair<std::decay_t<T>>::value;

	template<class T>struct is_tuple : std::false_type {};
	template<class ... Types>struct is_tuple<std::tuple<Types...>> : std::true_type {};
	template<class T>constexpr bool is_tuple_v = is_tuple<std::decay_t<T>>::value;

	template<class, class = void>struct has_begin_end : std::false_type {};
	template<class T>struct has_begin_end<T, std::void_t<decltype(std::declval<T>().begin()), decltype(std::declval<T>().end())>> : std::true_type {};
	template<class T>constexpr bool has_begin_end_v = has_begin_end<T>::value;

	//https://stackoverflow.com/questions/35293470/checking-if-a-type-is-a-map
	template<class, class = void>struct is_map :std::false_type {};
	template<class T>struct is_map<T, std::void_t<typename T::key_type, typename T::mapped_type, decltype(std::declval<T&>()[std::declval<const typename T::key_type&>()])>> :std::true_type {};
	template<class T>constexpr bool is_map_v = is_map<T>::value;

	template<class, class = void>struct has_size :std::false_type {};
	template<class T>struct has_size<T, std::void_t<decltype(std::declval<T>().size())>> :std::true_type {};
	template<class T>constexpr bool has_size_v = has_size<T>::value;

	template<class, class = void>struct is_indexable :std::false_type {};
	template<class T>struct is_indexable<T, std::void_t<decltype(std::declval<T>()[0])>> :std::true_type {};
	template<class T>constexpr bool is_indexable_v = is_indexable<T>::value;

	template<class ...Args>struct are_convertable :std::bool_constant<true> {};
	template<class To, class From>struct are_convertable<To, From> :std::bool_constant<std::is_convertible_v<From, To>> {};
	template<class To, class From, class ...Args>struct are_convertable<To, From, Args...> :std::bool_constant<std::is_convertible_v<From, To> && are_convertable<To, Args...>::value> {};
	template<class T, class ...Args>constexpr bool are_convertable_v = are_convertable<T, Args...>::value;

	template<class T, class Arr>struct is_c_array :std::false_type {};
	template<class T, size_t N>struct is_c_array<T, T[N]> :std::true_type {};
	template<class T, size_t N>struct is_c_array<T, T const[N]> :std::true_type {};
	template<class T, size_t N>struct is_c_array<T, T(&)[N]> :std::true_type {};
	template<class T, size_t N>struct is_c_array<T, T const(&)[N]> :std::true_type {};
	template<class T, class Arr>constexpr bool is_c_array_v = is_c_array<T, Arr>::value;

	namespace ToStreamDetails
	{
		constexpr wchar_t assoc_container_open_brace = L'{';
		constexpr wchar_t assoc_container_close_brace = L'{';

		constexpr wchar_t pair_open_brace = L'{';
		constexpr wchar_t pair_close_brace = L'}';

		constexpr wchar_t seq_beginend_container_open_brace = L'[';
		constexpr wchar_t seq_beginend_container_close_brace = L']';

		constexpr wchar_t indexalbe_container_open_brace = L'(';
		constexpr wchar_t indexalbe_container_close_brace = L')';

		constexpr wchar_t tuple_open_brace = L'<';
		constexpr wchar_t tuple_close_brace = L'>';

		constexpr wchar_t integer_sequence_open_brace = L'<';
		constexpr wchar_t integer_sequence_close_brace = L'>';

		template<wchar_t OpenBrace, wchar_t CloseBrace, class Tuple> void toStreamTuple(std::wostringstream&, Tuple&&);
		template<wchar_t OpenBrace, wchar_t CloseBrace, class Container> void toStreamBeginEnd(std::wostringstream&, Container&&);
		template<wchar_t OpenBrace, wchar_t CloseBrace, class Container> void toStreamIndexable(std::wostringstream&, Container&&);

		void toWstream(std::wostringstream&);

		template<class T, class...Args>
		void toWstream(std::wostringstream& wss, T&& value, Args&&...args);

		template<wchar_t OpenBrace, wchar_t CloseBrace, class Container>
		void toStreamBeginEnd(std::wostringstream& wss, Container&& value)
		{
			size_t size{ value.size() };
			if (size == 0)
				toWstream(wss, OpenBrace, CloseBrace);
			else
			{
				auto pFirst{ value.begin() };
				auto pEnd{ value.end() };

				toWstream(wss, OpenBrace, forward_like<Container>(*pFirst));

				++pFirst;
				while (pFirst != pEnd)
				{
					toWstream(wss, L',', forward_like<Container>(*pFirst));
					++pFirst;
				}
				toWstream(wss, CloseBrace);
			}
		}

		template<wchar_t OpenBrace, wchar_t CloseBrace, class Container>
		void toStreamIndexable(std::wostringstream& wss, Container&& value)
		{
			size_t size{ value.size() };
			if (size == 0)
				toWstream(wss, OpenBrace, CloseBrace);
			else
			{
				toWstream(wss, OpenBrace, forward_like<Container>(value[0]));
				for (size_t i{ 1 }; i < size; ++i)
					toWstream(wss, L',', forward_like<Container>(value[i]));
				toWstream(wss, CloseBrace);
			}
		}

		template<wchar_t OpenBrace, wchar_t CloseBrace, class T, T...I>
		void toStreamIntegerSequence(std::wostringstream& wss, std::integer_sequence<T, I...>)
		{
			toStreamTuple<OpenBrace, CloseBrace>(wss, std::make_tuple(I...));
		}

		template<wchar_t OpenBrace, wchar_t CloseBrace, class Tuple, size_t...I>
		void toStreamTupleEx(std::wostringstream& wss, Tuple&& value, std::index_sequence<I...>)
		{
			toWstream(wss, OpenBrace, forward_like<Tuple>(std::get<0>(value)));
			(..., toWstream(wss, L',', forward_like<Tuple>(std::get<I + 1>(value))));
			toWstream(wss, CloseBrace);
		}

		template<wchar_t OpenBrace, wchar_t CloseBrace, class Tuple>
		void toStreamTuple(std::wostringstream& wss, Tuple&& value)
		{
			constexpr size_t TupleSize{ std::tuple_size_v<Tuple> };
			if constexpr (TupleSize == 0)
				toWstream(wss, OpenBrace, CloseBrace);
			else
				toStreamTupleEx<OpenBrace, CloseBrace>(wss, std::forward<Tuple>(value), std::make_index_sequence<TupleSize - 1>{});
		}

		void toWstream(std::wostringstream&) {}

		template<class T, class...Args>
		void toWstream(std::wostringstream& wss, T&& value, Args&&...args)
		{
			using U = std::remove_cv_t<std::decay_t<T>>;
			//if constexpr(std::is_same_v<U, bool>)
			//	wss << (value ? L"True" : L"False");
			//else 
			if constexpr (std::is_same_v<std::nullptr_t, U>)
				wss << L"nullptr";
			else if constexpr (std::is_same_v<U, std::string>)
				wss << std::forward<T>(value).c_str();
			else if constexpr (is_streamable_v<std::wostringstream, T>)
				wss << std::forward<T>(value);
			else if constexpr (is_streamable_v<std::ostringstream, T>)
			{
				std::ostringstream ss;
				ss << std::forward<T>(value);
				wss << ss.str().c_str();
			}
			else if constexpr (is_pair_v<U>)
				toWstream(wss, pair_open_brace, forward_like<T>(std::forward<T>(value).first), L',', forward_like<T>(std::forward<T>(value).second), pair_close_brace);
			else if constexpr (is_tuple_v<U>)
				toStreamTuple<tuple_open_brace, tuple_close_brace>(wss, std::forward<T>(value));
			else if constexpr (has_begin_end_v<U> && !is_map_v<U>)
				toStreamBeginEnd<seq_beginend_container_open_brace, seq_beginend_container_close_brace>(wss, std::forward<T>(value));
			else if constexpr (has_begin_end_v<U> && is_map_v<U>)
				toStreamBeginEnd<assoc_container_open_brace, assoc_container_close_brace>(wss, std::forward<T>(value));
			else if constexpr (has_size_v<U>&&is_indexable_v<U>)
				toStreamIndexable<indexalbe_container_open_brace, indexalbe_container_close_brace>(wss, std::forward<T>(value));
			else if constexpr (is_integer_sequence_v<U>)
				toStreamIntegerSequence<integer_sequence_open_brace, integer_sequence_close_brace>(wss, std::forward<T>(value));
			else
				static_assert(std::is_same_v<T, T*>);//static_assert(false);
			toWstream(wss, std::forward<Args>(args)...);
		}
	}

	template<class...Args> void toWstream(std::wostringstream& wss, Args&&...args) { ToStreamDetails::toWstream(wss, std::forward<Args>(args)...); }
	template<class...Args> std::wstring toWstring(Args&&...args) { std::wostringstream wss; toWstream(wss, std::forward<Args>(args)...); return wss.str(); }

	void writeWchars(const wchar_t* str, size_t size) { WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), str, static_cast<DWORD>(size), nullptr, 0); }
	void writeWchars(const wchar_t* str) { WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), str, static_cast<DWORD>(wcslen(str)), nullptr, 0); }
	void writeChars(const char* str) { WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE), str, static_cast<DWORD>(strlen(str)), nullptr, 0); }

	template<size_t N>void writeCharArray(const char(&str)[N]) { WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE), str, N - 1, nullptr, 0); }
	template<size_t N>void writeWcharArray(const wchar_t(&str)[N]) { WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), str, N - 1, nullptr, 0); }

	void writeWchar(wchar_t c) { WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), &c, 1, nullptr, 0); }
	void writeChar(char c) { WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE), &c, 1, nullptr, 0); }

	template<class...Args> void writeEx(Args&&...args)
	{
		std::wostringstream ss;
		toWstream(ss, std::forward<Args>(args)...);
		std::wstring str{ ss.str() };
		writeWchars(str.c_str(), str.size());
	}
}


template<class ...Args> void write(Args&& ...args)
{
	using namespace writeln_details;

	constexpr size_t N{ sizeof...(args) };

	if constexpr (N == 0)
	{}
	else if constexpr (N == 1)
	{
		using type = std::remove_cv_t<writeln_details::nth_type_t<0, Args...>>;
		using decayed_type = std::decay_t<type>;

		if constexpr (std::is_same_v<char, decayed_type>)															writeChar(std::forward<Args>(args)...);
		else if constexpr (std::is_same_v<wchar_t, decayed_type>)													writeWchar(std::forward<Args>(args)...);
		else if constexpr (is_c_array_v<char, type>)																writeCharArray(std::forward<Args>(args)...);
		else if constexpr (is_c_array_v<wchar_t, type>)																writeWcharArray(std::forward<Args>(args)...);
		else if constexpr (std::is_same_v<const char*, decayed_type> || std::is_same_v<char*, decayed_type>)		writeChars(std::forward<Args>(args)...);
		else if constexpr (std::is_same_v<const wchar_t*, decayed_type> || std::is_same_v<wchar_t*, decayed_type>)	writeWchars(std::forward<Args>(args)...);
		else if constexpr (std::is_same_v<std::string, decayed_type>)												writeChars(std::forward<Args>(args).c_str()..., args.size()...);
		else if constexpr (std::is_same_v<std::wstring, decayed_type>)												writeWchars(std::forward<Args>(args).c_str()..., args.size()...);
		else																										writeEx(std::forward<Args>(args)...);
	}
	else
		writeEx(std::forward<Args>(args)...);
}

template<class ...Args> void writeln(Args&& ...args)
{
	write(std::forward<Args>(args)..., L'\n');
}
